/**
 * Created by justin on 5/10/16.
 */

var async = require('async'),
	keystone = require('keystone');

var Investment = keystone.list('Investment');
var User = keystone.list('User');

exports.post = function(req, res) {

	var item = new Investment.model(),
		body = (req.method == 'POST') ? req.body : req.query;
	var data = {};
	
	User.model.findOne().where({btcAddress:body.data.address}).exec(function(err, user) {
		if(user) {
			data.investor = user.id;
			data.referrer = user.referrer;
			data.btcAddress = body.data.address;
			data.btcAmount = body.additional_data.amount.amount;
			data.transID = body.additional_data.transaction.id;
			data.confirmed = true;
			
			item.getUpdateHandler(req).process(data, function(err) {

				if (err) return res.apiError('error', err);

				res.apiResponse({
					investment: item
				});

			})
		}
	});

	
};
