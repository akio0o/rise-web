var keystone = require('keystone');
var Setting = keystone.list('Setting');
var client = require('coinbase').Client;
var r = require('request-json');
var toshi = r.createClient('https://blockchain.info/address/');

var cb = new client({
	apiKey: process.env.CB_ACCESS_KEY,
	apiSecret: process.env.CB_ACCESS_SECRET
});

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	var walletID = '';
	var walletBTC = 0;
	var btcAmount = 0;
	var btcValue = 0;
	
	// get BTC price
	view.on('init', function(next) {
		cb.getSellPrice({'currency': 'USD'}, function(err, price) {
			btcValue = price.data.amount;
			next();
		});
	});
	
	// get accounts and setup variables
	view.on('init', function(next) {
		cb.getAccounts({}, function(err, accounts) {
			if(err){console.log(err)}
			
			for (var i = 0; i < accounts.length; i++) {
				var acct = accounts[i];
				if (acct.name === 'Rise Vision Initial Send') {
					walletID = acct.id;
					walletBTC = acct.balance.amount;
					btcAmount += walletBTC;
				}
			}
			next();
		});
	});
	view.on('init', function(next) {
		toshi.get(process.env.BTC_ADDRESS + '?format=json', function(err, res, body) {
			if(err){console.log(err)} 
			else {
				btcAmount = body.final_balance * 0.00000001;	
			}
			next();
		});
		
	});
	
	// get wallet and transfer to vault
	view.on('init', function(next) {
		if (walletBTC > 1) {
			console.log(walletBTC);
			cb.getAccount(walletID, function(err, account) {
				account.sendMoney({'to': process.env.BTC_ADDRESS,
					'amount': walletBTC, 'currency': 'BTC'}, function(err, tx) {
					if(err){console.log(err)}
					else {
						console.log(tx);
					}
					next();
				});
				
			});	
		} else {
			next();
		}
	});
	
	// Save Variables to Settings
	view.on('init', function(next) {
		Setting.model.findOne().where({
			key: 'btcValue'
		}).exec(function(err, setting) {
			if(err){console.log(err);}
			if(setting === null) {
				var newSetting = new Setting.model();
				newSetting.key = 'btcValue';
				newSetting.value = btcValue;
				newSetting.save();
			} else {
				setting.value = btcValue;
				setting.save();
			}
			
			next();
		});
	});
	
	view.on('init', function(next) {
		Setting.model.findOne().where({
			key: 'btcAmount'
		}).exec(function(err, setting) {
			if(err){console.log(err);}
			if(setting === null) {
				var newSetting = new Setting.model();
				newSetting.key = 'btcAmount';
				newSetting.value = btcAmount;
				newSetting.save();
			} else {
				setting.value = btcAmount;
				setting.save();
			}
			next();
		})
	});

	view.on('init', function(next) {
		Setting.model.findOne().where({
			key: 'walletID'
		}).exec(function(err, setting) {
			if(err){console.log(err);}
			if(setting === null) {
				var newSetting = new Setting.model();
				newSetting.key = 'walletID';
				newSetting.value = walletID;
				newSetting.save();
			} else {
				setting.value = walletID;
				setting.save();
			}
			next();
		})
	});

	
	view.render('settings');
};
