var keystone = require('keystone'),
	Types = keystone.Field.Types;

var PWReset = new keystone.List('PWReset', {});

PWReset.add({
	uuid: { type : Types.Relationship, ref : 'User' },
	expiration: { type: Types.Datetime },
	used: { type: Types.Boolean, default : false }
});

PWReset.defaultColumns = 'uuid, expiration, used';
PWReset.register();

